<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=414, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('admin.title') }}</title>
    <link rel="shortcut icon" href="/favicon.png" type="image/png">
    <link rel="stylesheet" href="{{ mix('css/admin.css') }}" type="text/css" />
</head>
<body>
<div class="container" id="app">
    <app></app>
</div>

@php
    $currencyService = app('currencies');
@endphp
<script>
	window.system = {
	    app_url: '{{ config('app.url') }}',
	    admin_prefix: '{{ setting('admin.url') }}',
		base_url: '{{ url('/api/admin') }}',
		timezone: '{{ config('app.timezone') }}',
		balances_type: '{{ setting('balances_type') }}',
		locale: '{{ App::getLocale() }}',
		csrf: '{{ csrf_token() }}',
		user: {
			id: {{ \Auth::id() ?? 0 }},
            token: '{{ $auth_token }}'
		},
		currencies: {
			default: @json($currencyService->getDefaultCurrency()),
			currencies: @json($currencyService->getCurrencies()),
			settings: {
				amount: @json(setting('currencies.amount'))
			},
		}
	}
</script>
<script src="{{ mix('js/admin.js') }}"></script>
</body>
</html>

