<?php

return [
    'title'                 => 'Список валют',
    'add_button_first'      => 'Добавить',
    'add_button_second'     => 'валюту',
    'update_button_first'   => 'Обновить',
    'update_button_second'  => 'курсы валют',
    'form'                  => [
        'create_title'      => 'Создание валюты',
        'update_title'      => 'Редактирование валюты',
        'required_help'     => '- поля обязательные для заполнения',
        'name'              => 'Название:',
        'symbol'            => 'Символ:',
        'code'              => 'Код:',
        'code_iso'          => 'Код ISO:',
        'code_iso_help'     => 'Код ISO 4217.',
        'rate'              => 'Курс',
        'rate_fixed_help'   => 'Курс для обмена. Поставьте галочку чтобы зафиксировать',
        'auto_update'       => 'Авто-обновление курса:',
        'submit'            => 'Сохранить',
    ]
];
