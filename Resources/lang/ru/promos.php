<?php

return [
    'main'  => [
        'monitoring'   => [
            'title'    => 'Мониторинги',
            'subtitle' => 'Расчет по всем мониторингам',
            'text'     => 'Ведите учет по всем мониторингам, которые вы приобрели',
        ],
        'blog'         => [
            'title'    => 'Блоги',
            'subtitle' => 'Расчет по всем блогам',
            'text'     => 'Ведите учет по всем блогам, которые вы приобрели',
        ],
        'banner'       => [
            'title'    => 'Баннерная реклама',
            'subtitle' => 'Расчет по каждому баннеру',
            'text'     => 'Ведите учет по всей баннерной рекламе, которая заказана Ваши',
        ],
        'youtube_blog' => [
            'title'    => 'Youtube блогеры',
            'subtitle' => 'Расчет по всем блогам',
            'text'     => 'Ведите учет по всем блогам, которые вы приобрели',
        ],
        'title'        => 'Полный учет вашего проекта',
        'subtitle'     => 'В данную колонку будет попадать вся статистика по по добавленным вами категориям',
        'total'        => 'Общий рекламный бюджет:',
        'sort'         => 'Быстрая сортировка:',
        'filters'      => [
            'all'        => 'Все',
            'monitoring' => 'Мониторинги',
            'blog'       => 'Блоги',
            'banner'     => 'Баннерная реклама',
            'youtube'    => 'Youtube блогеры',
        ],
    ],
    'pages' => [
        'total_budget' => 'Общий бюджет',
        'payback'      => 'Окупаемость:',
        'search'       => 'Поиск по названию...',
        'table'        => [
            'invested'          => 'Вложения:',
            'income'            => 'Принес:',
            'profit'            => 'Профит:',
            'structure'         => 'Структура:',
            'posted_at'         => 'Дата заказа:',
            'duration'          => 'Партнерство:',
            'withdraw'          => 'Всего выведено:',
            'cost'              => 'Цена:',
            'profit_help_white' => 'Профит - это',
            'profit_help'       => 'цифра, которая может быть как плюсовая так и минусовая, она зависит от цены листинга,
                    суммы инвестиций в структуре и суммы выводов реферальных и процентов, то есть эта цифра показывает окупил себя блогера или нет.',
        ],
        'monitoring'   => [
            'title'          => 'Мониторинги',
            'subtitle'       => 'Расчет по всем мониторингам',
            'three'          => 'Ведите учет по всем блогерам, которых вы заказали',
            'table_title'    => 'Все мониторинги',
            'table_subtitle' => 'Полный список мониторингов которые были заказаны',
            'total_count'    => 'Всего мониторингов:',
            'budget'         => [
                'first'  => 'Общая сумма вложенная в мониторинги:',
                'second' => 'Сумма инвестиций от мониторингов:',
            ],
            'modal'          => [
                'title'            => 'Добавление мониторинга',
                'name'             => 'Имя блогера:',
                'cost'             => 'Цена листинга:',
                'deposit'          => 'Депозит:',
                'deposit_included' => 'Депозит включен в листинг:',
                'posted_at'        => 'Дата размещения:',
                'user_id'          => 'ID пользователя:',
                'user'             => 'Пользователь:',
                'description'      => 'Описание (Заметки, особенности и тд):',
                'success'          => 'Мониторинг добавлен',
                'edit_title'       => 'Редактирование мониторинга',
                'edit_success'     => 'Мониторинг изменен',
            ],
        ],
        'blog'         => [
            'title'          => 'Блоги',
            'subtitle'       => 'Расчет по всем блогам',
            'three'          => 'Ведите учет по всем блогам, которых вы заказали',
            'table_title'    => 'Все блогеры',
            'table_subtitle' => 'Полный список блогов которые были заказаны',
            'total_count'    => 'Всего блогов:',
            'budget'         => [
                'first'  => 'Общая сумма вложений в блоги:',
                'second' => 'Сумма инвестиций от блогов:',
            ],
            'modal'          => [
                'title'            => 'Добавление блогера',
                'name'             => 'Имя блогера:',
                'cost'             => 'Цена листинга:',
                'deposit'          => 'Депозит:',
                'deposit_included' => 'Депозит включен в листинг:',
                'posted_at'        => 'Дата размещения:',
                'user_id'          => 'ID пользователя:',
                'user'             => 'Пользователь:',
                'description'      => 'Описание ( Заметки, особенности и тд):',
                'success'          => 'Блог добавлен',
                'edit_title'       => 'Редактирование блогера',
                'edit_success'     => 'Блогер изменен',
            ],
        ],
        'banner'       => [
            'title'          => 'Баннерная реклама',
            'subtitle'       => 'Расчет по всем баннерам',
            'three'          => 'Ведите учет по всем баннерам, которых вы заказали',
            'table_title'    => 'Все баннеры',
            'table_subtitle' => 'Полный список баннеров которые были заказаны',
            'total_count'    => 'Всего баннеров:',
            'budget'         => [
                'first'  => 'Общая сумма вложенная в баннеры:',
                'second' => 'Сумма инвестиций от баннеров:',
            ],
            'modal'          => [
                'title'        => 'Добавление рекламного баннера',
                'name'         => 'Название баннера:',
                'cost'         => 'Цена места:',
                'link'         => 'Ссылка на баннер:',
                'posted_at'    => 'Дата заказа:',
                'ended_at'     => 'Дата окончания:',
                'user_nick'    => 'Ник аккаунта реф. ссылки:',
                'user_id'      => 'ID пользователя:',
                'user'         => 'Пользователь:',
                'description'  => 'Описание ( Заметки, особенности и тд):',
                'success'      => 'Баннер добавлен',
                'edit_title'   => 'Редактирование баннера',
                'edit_success' => 'Баннер изменен',
            ],
        ],
        'video'        => [
            'title'          => 'Youtube блогеры',
            'subtitle'       => 'Расчет по всем блогерам',
            'three'          => 'Ведите учет по всем блогерам, которых вы заказали',
            'table_title'    => 'Все блогеры',
            'table_subtitle' => 'Полный список блогеров которые были заказаны',
            'total_count'    => 'Всего блогеров:',
            'budget'         => [
                'first'  => 'Общая сумма вложенная в блоги:',
                'second' => 'Сумма инвестиций от блогеров:',
            ],
            'modal'          => [
                'title'            => 'Добавление блогера',
                'name'             => 'Имя блогера:',
                'cost'             => 'Цена листинга:',
                'deposit'          => 'Депозит:',
                'deposit_included' => 'Депозит включен в листинг:',
                'posted_at'        => 'Дата размещения:',
                'user_id'          => 'ID пользователя:',
                'user'             => 'Пользователь:',
                'description'      => 'Описание ( Заметки, особенности и тд):',
                'success'          => 'Youtube блог добавлен',
                'edit_title'       => 'Редактирование Youtube блога',
                'edit_success'     => 'Youtube блог изменен',
            ],
        ],
    ],
];
