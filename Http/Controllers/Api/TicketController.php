<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Api\CreateTicketRequest;
use Modules\Admin\Http\Requests\Api\TicketMessageRequest;
use Modules\Core\Models\Ticket;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator
    {
        return app('zengine')->model('Ticket')->with([
            'messages.user',
            'user',
        ])->latest('last_message_at')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     * @param  CreateTicketRequest  $request
     * @return array
     */
    public function store(CreateTicketRequest $request) : array
    {
        $ticket_data = $request->only([
            'user_id',
            'subject',
            'status',
        ]);
        $ticket = Ticket::create($ticket_data);
        $ticket->messages()->create([
            'user_id' => \Auth::id(),
            'message' => $request->get('message'),
        ]);

        return [
            'status' => 'success',
            'ticket' => $ticket->load('messages'),
        ];
    }

    /**
     * Show the specified resource.
     * @param  int  $ticketId
     * @return Ticket
     */
    public function show($ticketId)
    {
        $ticket = app('zengine')->model('Ticket')->with([
            'messages' => function ($q) {
                $q->with('user')->oldest();
            },
            'user',
        ])->findOrFail($ticketId);
        $ticket->messages->where('read', 0)->where('user_id', $ticket->user_id)->map(function ($message) {
            $message->read = 1;
            $message->save();
        });
        return $ticket;
    }

    public function message(TicketMessageRequest $request)
    {
        $ticket = app('zengine')->model('Ticket')->findOrFail($request->get('ticket_id'));
        if ($request->has('message_id')) {
            $message = app('zengine')->model('TicketMessage')->findOrFail($request->get('message_id'));
        } else {
            $message = $ticket->messages()->make([
                'user_id' => \Auth::id(),
            ]);
        }
        $message->message = $request->get('message');

        return [
            'status'         => $message->save() ? 'success' : 'error',
            'message'        => $request->get('message_id') ? 'Сообщение обновлено' : 'Сообщение отправлено',
            'ticket_message' => $message->load('user'),
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $ticketId
     * @return string[]
     * @throws \Exception
     */
    public function destroy($ticketId)
    {
        $ticket = app('zengine')->model('Ticket')->with(['messages.user', 'user'])->findOrFail($ticketId);
        return [
            'status' => $ticket->delete() ? 'success' : 'error',
        ];
    }
}
