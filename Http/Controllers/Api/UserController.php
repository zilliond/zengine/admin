<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Admin\Http\Requests\Api\CreateUserRequest;
use Modules\Admin\Http\Requests\Api\SendMessageToTicketRequest;
use Modules\Admin\Http\Requests\Api\UpdateUserRequest;
use Modules\Admin\Http\Requests\Api\UserSendMailRequest;
use Modules\Core\Models\TicketMessage;
use Modules\Core\Models\User;

class UserController extends Controller
{
    /**
     * @param  $id
     * @return User
     */
    public function show($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return $user->loadMissing(['currency', 'inviter', 'wallets.payment_system']);
    }

    /**
     * @return array
     */
    public function balances()
    {
        $users = app('zengine')->model('User')->with('currency')->get();
        $currencyService = app('currencies');
        $balance = $users->reduce(function ($balance, User $user) use ($currencyService) {
            return $balance + $currencyService->convertToDefault($user->balance, $user->currency);
        }, 0);

        return [
            'balance' => $balance
        ];
    }

    /**
     * @return array
     */
    public function count()
    {
        return [
            'total' => app('zengine')->model('User')->count()
        ];
    }

    /**
     * @param  CreateUserRequest  $request
     * @return array
     */
    public function create(CreateUserRequest $request)
    {
        $data = $request->only(['login', 'email']) + [
            'name'              => $request->get('login'),
            'password'          => \Hash::make($request->get('password')),
            'email_verified_at' => now()
        ];
        $user = app('zengine')->model('User')->create($data);

        return [
            'status' => 'success',
            'user'   => $user
        ];
    }

    /**
     * @param $id
     * @param  UpdateUserRequest  $request
     * @return string[]
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        if ($request->get('password')) {
            $user->password = \Hash::make($request->get('password'));
            \Debugbar::info('password update');
        }
        $user->fill($request->only([
            'email',
            'balance',
            'currency_id',
            'referral_percent',
            'comment',
        ]));
        $wallets_data = $request->get('wallets');
        $wallets = $user->wallets()->pluck('wallet', 'payment_system_id');
        foreach ($wallets_data as $ps_id => $value) {
            if (!$value) {
                continue;
            }
            $payment_system = app('zengine')->model('PaymentSystem')->findOrFail((int) $ps_id);
            if (isset($wallets[$payment_system->id]) && $wallets[$payment_system->id] === $value) {
                continue;
            }
            $user->wallets()->updateOrCreate([
                'payment_system_id' => $payment_system->id
            ], [
                'wallet' => $value
            ]);
        }
        $user->save();

        return [
            'status' => 'success'
        ];
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $users = app('zengine')->model('User')->with(['currency'])
            ->when($search, function (Builder $builder) use ($search) {
                return $builder
                    ->where('id', $search)
                    ->orWhere('login', 'LIKE', "%$search%")
                    ->orWhere('email', "$search")
                    ->orWhereHas('wallets', function (Builder $q) use ($search) {
                        $q->where('wallet', $search);
                    });
            })
            ->paginate();
        \Debugbar::info($users);

        return $users;
    }

    public function search(Request $request)
    {
        return app('zengine')->model('User')->
            when(request('login'), static function (Builder $builder) {
                return $builder->where('login', 'LIKE', '%'.request('login').'%');
            })
            ->when(request('limit'), static function (Builder $builder) {
                return $builder->limit(\request('limit'));
            })
            ->get();
    }

    public function shortDeposits($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return $user->deposits()->with(['plan', 'currency'])->latest()->paginate(5);
    }

    public function shortRefills($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $amounts = $user->operations()->with(['currency'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->groupBy(['currency_id'])->selectRaw(\DB::raw('sum(amount) as sum, currency_id'))->pluck('sum', 'currency_id');
        $operations = $user->operations()->with(['currency', 'payment_system.currency', 'wallet'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->latest()
            ->paginate(5);
        return [
            'operations' => $operations,
            'amounts'    => $amounts,
        ];
    }

    public function shortWithdraws($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $amounts = $user->operations()->with(['currency'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->groupBy(['currency_id'])->selectRaw(\DB::raw('sum(amount) as sum, currency_id'))->pluck('sum', 'currency_id');
        $operations = $user->operations()->with(['currency', 'payment_system.currency', 'wallet'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->latest()
            ->paginate(5);
        return [
            'operations' => $operations,
            'amounts'    => $amounts,
        ];
    }

    public function shortReferrals($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $service = app('users');
        $referrals = $service->getFlattenReferrals($user);
        return $referrals->map(function (User $referral) use ($service, $user) {
            $row = $referral->toArray();
            $row['earned'] = $service->getReferralEarned($user, $referral);
            return $row;
        });
    }

    public function shortAuthLogs($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return $user->auth_logs()->latest()->paginate(5);
    }

    public function shortTickets($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return $user->tickets()->latest('last_message_at')->paginate(5);
    }

    public function shortMessages($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $tickets_id = $user->tickets()->pluck('id');
        return app('zengine')->model('TicketMessage')->with(['user', 'ticket'])->whereIn('ticket_id', $tickets_id)->latest()->paginate(5);
    }

    public function tickets($id)
    {
        return app('zengine')->model('User')->findOrFail($id)->tickets;
    }

    public function sendMessage($id, SendMessageToTicketRequest $request)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $from = app('zengine')->model('User')->where('login', $request->get('from'))->firstOrFail();
        \Debugbar::info($request->all());
        $ticket = $user->tickets()->create($request->only(['subject']));
        $ticket->messages()->create([
            'user_id' => $from->id,
            'message' => $request->get('message')
        ]);
        return [
            'status' => 'success'
        ];
    }

    public function mail($id, UserSendMailRequest $request)
    {
        /** @var User $user */
        $user = app('zengine')->model('User')->findOrFail($id);
        Mail::raw($request->get('message'), function ($message) use ($request, $user) {
            /** @var Message $message */
            $message->from($request->get('from', setting('general.admin_mail')))
                ->to($user->email);
        });
        return [
            'status' => 'success'
        ];
    }

    public function sendMessageAll(SendMessageToTicketRequest $request)
    {
        $from = app('zengine')->model('User')->where('login', $request->get('from'))->firstOrFail();
        app('zengine')->model('User')->chunk(50, function ($users) use ($request, $from) {
            foreach ($users as $user) {
                $ticket = $user->tickets()->create($request->only(['subject']));
                $ticket->messages()->create([
                    'user_id' => $from->id,
                    'message' => $request->get('message')
                ]);
            }
            unset($users);
        });
        return [
            'status' => 'success'
        ];
    }

    /**
     * @param  TicketMessage  $message
     * @return array
     * @throws \Exception
     */
    public function deleteMessage(TicketMessage $message)
    {
        return [
            'status' => $message->delete() ? 'success' : 'error'
        ];
    }

    public function referrals($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $service = app('users');
        $referrals = $service->getFlattenReferrals($user);
        return $referrals->map(function (User $referral) use ($service, $user) {
            $row = $referral->toArray();
            $row['earned'] = $service->getReferralEarned($user, $referral);
            return $row;
        });
    }

    public function payment_systems($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return app('zengine')->model('PaymentSystem')->with([
            'wallets' => function ($query) use ($user) {
                return $query->where('user_id', $user->id);
            }
        ])->where('need_wallet', 1)->get();
    }

    public function wallets($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return app('zengine')->model('PaymentSystem')->all()->map(static function ($paymentSystem) use ($user) {
            $wallet = app('zengine')->model('Wallet')->firstOrCreate([
                'payment_system_id' => $paymentSystem->id,
                'user_id'           => $user->id
            ], [
                'balance' => 0
            ]);
            $wallet->load('payment_system');
            return $wallet;
        });
    }

    public function storeWallets($id, Request $request)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        \Debugbar::info($request->all());
        $wallets = $request->get('wallets');
        if (is_array($wallets)) {
            foreach ($wallets as $wallet_data) {
                $wallet = $user->wallets()->firstOrCreate([
                    'payment_system_id' => $wallet_data['payment_system_id'],
                    'user_id'           => $wallet_data['user_id'],
                ]);
                $wallet->balance = $wallet_data['balance'] ?? 0;
                $wallet->wallet = $wallet_data['wallet'] ?? '';
                $wallet->save();
            }
            return [
                'status' => 'success'
            ];
        }
        return [
            'status' => 'error'
        ];
    }

    /**
     * @param  User  $user
     * @return array
     * @throws \Exception
     */
    public function delete($id)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        return [
            'status' => $user->delete() ? 'success' : 'error'
        ];
    }

    /**
     * @param  User  $user
     * @param  Request  $request
     * @return array
     */
    public function setRole($id, Request $request)
    {
        $user = app('zengine')->model('User')->findOrFail($id);
        $user->role = (int) $request->get('role');
        $user->save();
        return [
            'status' => 'success'
        ];
    }
}
