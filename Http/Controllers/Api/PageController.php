<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PageController extends Controller
{
    public function index()
    {
        return app('zengine')->model('Page')->paginate();
    }

    public function show($id)
    {
        return app('zengine')->model('Page')->findOrFail($id);
    }

    public function store(Request $request)
    {
        $page = app('zengine')->model('Page')->create($request->all());
        return [
            'status' => 'success',
            'page'   => $page
        ];
    }

    public function update($id, Request $request)
    {
        $page = app('zengine')->model('Page')->findOrFail($id);
        $page->fill($request->all());
        $page->save();
        return [
            'status' => 'success',
            'page'   => $page
        ];
    }

    public function delete($id)
    {
        $page = app('zengine')->model('Page')->findOrFail($id);
        return [
            'status' => $page->delete() ? 'success' : 'error'
        ];
    }
}
